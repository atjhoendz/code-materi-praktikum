$(document).ready(function() {
    $('#konten_wrapper').hide();

    $('#getDataBtn').click(function() {
        $.ajax({
            url: 'https://reqres.in/api/users?page=1',
            type: 'GET',
            dataType: 'json',
        })
        .done(function(data) {
            $('#konten_wrapper').show();
            $.each(data.data, function(index, el) {
                $('#konten').append('<div class="card shadow p-3">'+
                '                        <img class="card-img-top img-rounded" src="'+ el.avatar +'" alt="Card image cap">'+
                '                        <div class="card-block">'+
                '                          <h4 class="card-title">'+ el.first_name + ' ' + el.last_name +'</h4>'+
                '                          <p class="card-text">'+ el.email +'</p>'+
                '                        </div>'+
                '                      </div>')


            });
            $('#getDataBtn').hide();


        })
        .fail(function() {
            console.log("error");
        })
    });
});
